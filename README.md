# Patch Flap Tileset

This provides a tileset for use with [PAYGE, the Pandora and Yates Game Engine](https://gitlab.com/aquarichy/payge).
You can see it in use in the demo game [Patch Friends](https://gitlab.com/aquarichy/patch-friends).

<img title="Town" alt="Town" src="doc/1_town.png" width="320" /> <img title="Pond" alt="Pond" src="doc/2_pond.png" width="320" />

<img title="Cafe" alt="Cafe" src="doc/3_cafe.png" width="320" /> <img title="Gallery" alt="Gallery" src="doc/4_gallery.png" width="320" />

<img title="Tiles" alt="Tiles" src="doc/collage.1024.png" width="512" height="512" />

## Tools

Check out a visual index of defined tiles and their properties here: [aquarichy.gitlab.io/patch-flap-tileset](https://aquarichy.gitlab.io/patch-flap-tileset/)

<img title="Tile index" alt="Tile index" src="doc/10_index.png" width="320" /> <img title="Tile index with rand set" alt="Tile index with rand set" src="doc/11_index_randset.png" width="320" />

Check out a test map generator here: [aquarichy.gitlab.io/patch-flap-tileset/map_generator](https://aquarichy.gitlab.io/patch-flap-tileset/map_generator/)

<img title="Map Generator" alt="Map Generator" src="doc/20_mapgen.png" width="512" />

## Tiles


Tiles are 16x16 PNG images, often with translucent backgrounds so they can be
layered together.

Source files are generally XCF files from GIMP.  Some are actually 64x64 tiles
for when we want to do something with more pixel sand then downscale it to be
16x16 for actual usage.  Or in case we want to support larger tiles in the
future.

Colours were originally recommended to be restricted to 1/16th the space.
So #775500 works, but not #735B02. However, I've stopped following that. 🙃

## Directory structure and Tile Group ID

Tiles are grouped thematically.  Tile groupings are also numbered reflecting
the directory path to the tile.  E.g. for the a south-end cherry oak counter-top,
the Tile Group ID would be 43021, reflecting this directory path:

* 4.inside/3.furniture/02.counter/1.cherry oak/43021.s.png

## tilemap.js and Tile ID

Each tile (or specified combination) has a unique ID distinct from the Tile Group ID.
These are sequentially assigned integers, and are for use with the PAYGE map grid.

Tile IDs are defined in tilemap.js, as well as properties on those tiles. To learn more about the properties,
you can look inside tilemap.js or review the table below.

## Tile Properties

As of 2023-04-21:

<table>
<thead>
<tr>
<th>Property</th><th>Value Type</th><th>Description</th>
</tr>
</thead>
<tbody>
<tr><td> go           </td><td> bool       </td><td> true if a unit can walk on the tile, false if not</td></tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr><td> src          </td><td> string     </td><td> The path in the tileset to a given tile PNG image</td></tr>
<tr><td> <em>OR</em> src      </td><td> array      </td><td> Combination of tiles, layered from lowest (i=0) to highest as i increases</td></tr>
<tr><td> <em>OR</em> rand_set </td><td> array      </td><td> A list of other tile IDs, one of which will be selected and used in place of this tile</td></tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr><td colspan="3"><em>Optional</em></td></tr>
<tr><td> z            </td><td> int        </td><td> z-index for the tile on the map, to override the default.  Defaults: if go: true, then z: 0 (below the player unit.  If go: false, then z: 2 (above the player unit)</td></tr>
<tr><td> battle       </td><td> float      </td><td> Chance of encountering a battle on said tile. (TODO separate this from the tile</td></tr>
<tr><td> teleport     </td><td> bool       </td><td> If entering this tile should teleport the player unit (e.g. a doorway</td></tr>
<tr><td> bump         </td><td> bool       </td><td> If the unit player can interact directly with it. (E.g. a sign pops up information</td></tr>
<tr><td> filter       </td><td> string     </td><td> Apply a filter function to the appearance of the tile, possibility dependent on an RNG or</td></tr>
</tbody>
</table>

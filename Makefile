all: build

build: build/tiles

build/tiles: src/tiles
	test -d build || mkdir build
	test -d build/tiles && rm -rf build/tiles || true
	cd src/tiles; \
	find . -type d -exec install -d -D ../../build/tiles/{} \; ; \
	find . -type f -name "*.png" -exec install -D {} ../../build/tiles/{} \; ;
	cp src/*.js build/
	touch build/tiles

clean:
	rm -rf build/ || true
	rm doc/collage.png 2>/dev/null || true
	find . -name "*~" -delete

dist: dist/patch-flap.tar.gz

dist/patch-flap.tar.gz: build/tiles
	test -d dist || mkdir dist
	cp -prd build dist/patch-flap
	cd "dist" && tar -czf "patch-flap.tar.gz" "patch-flap" && rm -rf "patch-flap/"
	cd "dist" && cp "patch-flap.tar.gz" "patch-flap.$$(date -Is).tar.gz"

distclean: clean
	rm -rf dist/ || true

doc: doc/collage.png

doc/collage.png: build/tiles
	mkdir -p /tmp/pngs/small
	find build/tiles -name "*.png" | \
		while read OLDPATH; do \
			echo "$${OLDPATH}" | tr '/' '_' | while read NEWFILE; do \
				cp "$${OLDPATH}" "/tmp/pngs/$${NEWFILE}" && \
				convert "/tmp/pngs/$${NEWFILE}" -resize 16x16 "/tmp/pngs/small/$${NEWFILE}"; \
			done ; \
		done;
	montage /tmp/pngs/small/*.png -tile 16x16 -geometry +0+0 -background none doc/collage.png


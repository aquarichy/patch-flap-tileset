#!/bin/sh

LAST="$(grep -P "^ +[0-9]+:" tilemap.js | cut -d : -f 1 | sed -r -e 's;^ +;;g' | sort -n | tail -n 1)";
let NEXT=${LAST}+1;

echo "Last ID: ${LAST}"
echo "Next ID: ${NEXT}";


const c_water_land_road_repeat = 20;
const water_corner_repeat = 3;
const water_x_of_land_repeat = 3;

/* water */
let for_w_of_water_c = repeat (158, c_water_land_road_repeat).concat ([ 155, 173, 176 ]);
let for_w_of_water_n = [ 153, 153, 154, 175 ]
let for_w_of_water_s = [ 149, 149, 148, 174 ];
let for_e_of_water_c = repeat (158, c_water_land_road_repeat).concat ([ 151, 174, 175 ]);
let for_e_of_water_n = [ 153, 153, 152, 176 ]
let for_e_of_water_s = [ 149, 149, 150, 173 ];

let for_n_of_water_c = repeat (158, c_water_land_road_repeat).concat ([ 153, 176, 175 ]);
let for_n_of_water_w = [ 155, 155, 154, 173 ];
let for_n_of_water_e = [ 151, 151, 152, 174 ];
let for_s_of_water_c = repeat (158, c_water_land_road_repeat).concat ([ 149, 173, 174 ]);
let for_s_of_water_w = [ 155, 155, 148, 176 ];
let for_s_of_water_e = [ 151, 151, 150, 175 ];

/* land */
let land_c = repeat (147, c_water_land_road_repeat).concat (repeat (270, 10));

/* a list of tiles to exclude if we want to limit, say, roads */
               /* path ends */        /* path 3-ways */
let exclude = [  165, 166, 167, 168,    181, 182, 183, 184,   178, 180 ];
/* TODO: let this be configurable */


/*                     land_c                water                     road                            tree   */
let for_w_of_land_c = land_c.concat (repeat ([ 152, 151, 150 ], water_x_of_land_repeat), [165, 167, 166, 169, 177, 180, 181], repeat ([271, 272, 273, 274], 2))._subtract (exclude);
let for_n_of_land_c = land_c.concat (repeat ([ 148, 149, 150 ], water_x_of_land_repeat), [166, 165, 168, 170, 180, 179, 184], repeat ([271], 2))._subtract (exclude);
let for_e_of_land_c = land_c.concat (repeat ([ 154, 155, 148 ], water_x_of_land_repeat), [165, 167, 168, 169, 178, 179, 183], repeat ([271, 272, 273, 274], 2))._subtract (exclude);
let for_s_of_land_c = land_c.concat (repeat ([ 154, 153, 152 ], water_x_of_land_repeat), [166, 167, 168, 170, 177, 178, 182], repeat ([272, 274], 2))._subtract (exclude);

/* path */
let for_w_of_path_e = [ 168, 170, 171, ...repeat(178, 3), ...repeat(179, 3), 183, 182, 184, ...repeat (170, c_water_land_road_repeat*10) ]._subtract (exclude);
let for_e_of_path_w = [ 166, 170, 171, ...repeat(180, 3), ...repeat(177, 3), 181, 182, 184, ...repeat (170, c_water_land_road_repeat*10) ]._subtract (exclude);
let for_n_of_path_s = [ 167, 169, 171, ...repeat(177, 3), ...repeat(178, 3), 181, 183, 182, ...repeat (169, c_water_land_road_repeat*10) ]._subtract (exclude);
let for_s_of_path_n = [ 165, 169, 171, ...repeat(179, 3), ...repeat(180, 3), 181, 183, 184, ...repeat (169, c_water_land_road_repeat*10) ]._subtract (exclude);

let paths = [ 165, 166, 167, 168, 169, 170, 171, 177, 178, 179, 180, 181, 182, 183, 184 ]._subtract (exclude);

land_c_dirs = { right: for_e_of_land_c,
                left:  for_w_of_land_c,
                up:    for_n_of_land_c,
                down:  for_s_of_land_c };



const neighbours = {
  /* nature */
  147:  land_c_dirs, /* grass+stone land */
  270:  land_c_dirs, /* bush */

  /* trees */
  /*   s */
  271: { right: for_e_of_land_c,
         left:  for_w_of_land_c,
         up:    [ 272, 273, 273, 273 ],
         down:  for_s_of_land_c, },
  /*   n(for s) */
  272: { right: for_e_of_land_c,
         left:  for_w_of_land_c,
         up:    for_n_of_land_c,
         down:  [ 271 ], },
  /*   c(for s) */
  273: { right: for_e_of_land_c,
         left:  for_w_of_land_c,
         up:    [ 274 ],
         down:  [ 271 ], },
  /*   n(for c) */
  274: { right: for_e_of_land_c,
         left:  for_w_of_land_c,
         up:    for_n_of_land_c,
         down:  [ 273 ], },

  /*** paths ***/
  /* s */
  165: { right: subtract (for_e_of_land_c, paths),
         left:  subtract (for_w_of_land_c, paths),
         up:    for_n_of_path_s,
         down:  subtract (for_s_of_land_c, paths), },
  /* e */
  166: { right: subtract (for_e_of_land_c, paths),
         left:  for_w_of_path_e,
         up:    subtract (for_n_of_land_c, paths),
         down:  subtract (for_s_of_land_c, paths), },
  /* n */
  167: { right: subtract (for_e_of_land_c, paths),
         left:  subtract (for_w_of_land_c, paths),
         up:    subtract (for_n_of_land_c, paths),
         down:  for_s_of_path_n, },
  /* w */
  168: { right: for_e_of_path_w,
         left:  subtract (for_w_of_land_c, paths),
         up:    subtract (for_n_of_land_c, paths),
         down:  subtract (for_s_of_land_c, paths), },
  /* v | */
  169: { right: subtract (for_e_of_land_c, paths),
         left:  subtract (for_w_of_land_c, paths),
         up:    for_n_of_path_s,
         down:  for_s_of_path_n, },
  /* h - */
  170: { right: for_e_of_path_w,
         left:  for_w_of_path_e,
         up:    subtract (for_n_of_land_c, paths),
         down:  subtract (for_s_of_land_c, paths), },
  /* + */
  171: { right: [ 170 ],
         left:  [ 170 ],
         up:    [ 169 ],
         down:  [ 169 ], },

  /* s-w */
  177: { right: subtract (for_e_of_land_c, paths),
         left:  [ 170 ],
         up:    subtract (for_n_of_land_c, paths),
         down:  [ 169 ], },
  /* s-e */
  178: { right: [ 170 ],
         left:  subtract (for_w_of_land_c, paths),
         up:    subtract (for_n_of_land_c, paths),
         down:  [ 169 ], },
  /* n-e */
  179: { right: [ 170 ],
         left:  subtract (for_w_of_land_c, paths),
         up:    [ 169 ],
         down:  subtract (for_s_of_land_c, paths), },
  /* n-w */
  180: { right: subtract (for_e_of_land_c, paths),
         left:  [ 170 ],
         up:    [ 169 ],
         down:  subtract (for_s_of_land_c, paths), },
  /* n-w-s */
  181: { right: subtract (for_e_of_land_c, paths),
         left:  [ 170 ],
         up:    [ 169 ],
         down:  [ 169 ], },
  /* w-s-e */
  182: { right: [ 170 ],
         left:  [ 170 ],
         up:    subtract (for_n_of_land_c, paths),
         down:  [ 169 ], },
  /* n-e-s */
  183: { right: [ 170 ],
         left:  subtract (for_w_of_land_c, paths),
         up:    [ 169 ],
         down:  [ 169 ], },
  /* w-n-e */
  184: { right: [ 170 ],
         left:  [ 170 ],
         up:    [ 169 ],
         down:  subtract (for_s_of_land_c, paths), },

  /*** water ***/
  /* nw */
  154: { right: for_e_of_water_n.concat (repeat (176, water_corner_repeat)),
         left:  for_w_of_land_c,
         up:    for_n_of_land_c,
         down:  for_s_of_water_w.concat (repeat (176, water_corner_repeat)) },
  /* w  */
  155: { right: for_e_of_water_c,
         left:  for_w_of_land_c,
         up:    for_n_of_water_w,
         down:  for_s_of_water_w },
  /* sw  */
  148: { right: for_e_of_water_s.concat (repeat (173, water_corner_repeat)),
         left:  for_w_of_land_c,
         up:    for_n_of_water_w.concat (repeat (173, water_corner_repeat)),
         down:  for_s_of_land_c },

  /* ne  */
  152: { right: for_e_of_land_c,
         left:  for_w_of_water_n.concat (repeat (175, water_corner_repeat)),
         up:    for_n_of_land_c,
         down:  for_s_of_water_e.concat (repeat (175, water_corner_repeat)) },
  /* n  */
  153: { right: for_e_of_water_n,
         left:  for_w_of_water_n,
         up:    for_n_of_land_c,
         down:  for_s_of_water_c },

  /* c  */
  158: { right: for_e_of_water_c,
         left:  for_w_of_water_c,
         up:    for_n_of_water_c,
         down:  for_s_of_water_c },
  /* e  */
  151: { right: for_e_of_land_c,
         left:  for_w_of_water_c,
         up:    for_n_of_water_e,
         down:  for_s_of_water_e },
  /* se */
  150: { right: for_e_of_land_c,
         left:  for_w_of_water_s.concat (repeat (174, water_corner_repeat)),
         up:    for_n_of_water_e.concat (repeat (174, water_corner_repeat)),
         down:  for_s_of_land_c },
  /* s */
  149: { right: for_e_of_water_s,
         left:  for_w_of_water_s,
         up:    for_n_of_water_c,
         down:  for_s_of_land_c },

  /* sw in */
  173: { right: for_e_of_water_c,
         left:  for_w_of_water_s.concat (repeat (148, water_corner_repeat)),
         up:    for_n_of_water_c,
         down:  for_s_of_water_w.concat (repeat (148, water_corner_repeat)), },
  /* se in */
  174: { right: for_e_of_water_s.concat (repeat (150, water_corner_repeat)),
         left:  for_w_of_water_c,
         up:    for_n_of_water_c,
         down:  for_s_of_water_e.concat (repeat (150, water_corner_repeat)), },
  /* ne in */
  175: { right: for_e_of_water_n.concat (repeat (152, water_corner_repeat)),
         left:  for_w_of_water_c,
         up:    for_n_of_water_e.concat (repeat (152, water_corner_repeat)),
         down:  for_s_of_water_c },
  /* nw in */
  176: { right: for_e_of_water_c,
         left:  for_w_of_water_n.concat (repeat (154, water_corner_repeat)),
         up:    for_n_of_water_w.concat (repeat (154, water_corner_repeat)),
         down:  for_s_of_water_c },

};


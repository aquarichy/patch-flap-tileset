/* Override Math.random with mulberry32

   Original C implementation by Tommy Ettinger:
   https://gist.github.com/tommyettinger/46a874533244883189143505d203312c
   JavaScript implementation used here by bryc:
   https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript#Mulberry32
 */
function mulberry32(a) {
  return function() {
    var t = a += 0x6D2B79F5;
    t = Math.imul(t ^ t >>> 15, t | 1);
    t ^= t + Math.imul(t ^ t >>> 7, t | 61);
    return ((t ^ t >>> 14) >>> 0) / 4294967296;
  }
}

function math_random_use_mulberry32 () {
  const seed = Math.floor (Math.random () * Math.pow (2,32));
  Math.random = mulberry32(seed);
  console.log (`mulberry32 seed: ${seed}`);

  return seed;
}

Array.prototype._subtract = function (to_exclude) {
  return this.filter ((x) => !to_exclude.includes (x))
};

function subtract (array1, array2) {
  return array1.filter (x => !array2.includes (x));
}

/**
 * repeat:
 *
 * Adds `value` to a list `times` times.  If `value` is an array, it adds each of its contents `times` times.
 *
 * @param value string   The value (or array of values) to be added `times` times.
 * @param times int      The number of times the value should be added to the list.
 */
function repeat (value, times) {
  let list = [];
  for (let i = 0; i < times; i++) {
    if (value instanceof Array) {
      list.push (...value);
    } else {
      list.push (value);
    }
  }
  return list;
}

function list_get_rand (list) {
  return list[Math.floor (list.length * Math.random ())];
}

function display_tile (y, x, src, id) {
  if (src instanceof Array) {
    for (let id of src) {
      let tile = get_tile (id);
      display_tile (y, x, tile.src, id);
    }

    return;
  }

  let img = document.createElement ("IMG");
  img.src = "../tiles/" + src;
  img.setAttribute ("x", x);
  img.setAttribute ("y", y);
  img.setAttribute ("tid", id);

  Object.assign (img.style, {
    position: "absolute",
    width: `${g_size}${g_unit}`,
    height: `${g_size}${g_unit}`,
    // outline: "1px solid black",
    top: (y*g_size)+g_unit,
    left: (x*g_size)+g_unit,
  });
  document.body.querySelector ("#screen").appendChild (img);
}

function get_tile (id) {
  let tile = tile_map[id];

  // console.log (`id: ${id}, tile: ${tile}`);

  if (tile.rand_set) {
    id = list_get_rand (tile.rand_set);
    return get_tile (id);
  }

  return tile;        
}

/**
 * pad:
 *
 * Adds numeric `num` to be 3 chars wide.  Just supports positives of two-digit
 * and one digit, and negatives of one digit.
 *
 * @param num number   The number to pad.
 *
 * @return string   The padded string representation of the number.
 */
function pad (num) {
  num = parseInt (num);
  if (num < 0) {
    return ` ${num}`;
  } else if (num < 10) {
    return `  ${num}`;
  } else if (num < 100) {
    return ` ${num}`;
  } else {
    return num;
  }
}

function display_screen_text (screen) {
  let msg = "";

  msg += " y\\x";
  for (let x = 0; x < screen[0].length; x++) {
    msg += "" + pad (x) + "   ";
  }
  msg += "\n";

  for (let y = 0; y < screen.length; y++) {
    msg += pad (y) + ": ";
    for (let x = 0; x < screen[y].length; x++) {
      let id = pad (screen[y][x]);
      msg += ` ${id}, `;
    }
    msg += "\n";
  }

  console.log ("Screen:");
  console.log (msg);
}

function display_screen (use_manual_seed = false) {
  const map_generate_func = funcs[document.getElementById ("algorithm").value];
  const seed_input = document.getElementById ("seed");
  const tiles_wide = document.getElementById ("tiles_wide").value;
  const tiles_high = document.getElementById ("tiles_high").value;

  if (use_manual_seed) {
    Math.random = mulberry32 (parseInt (seed_input.value));
  } else {
    seed_input.value = math_random_use_mulberry32 ();
  }

  const map = map_generate_func (tiles_wide, tiles_high);

  display_screen_text (map);
  display_screen_imgs (map);
}

function display_screen_imgs (map) {
  const screen_div = document.getElementById ("screen");

  Object.assign (document.querySelector ("#screen").style, {
    width: (map[0].length * g_size) + g_unit,
    height: (map.length * g_size) + g_unit,
  });

  screen_div.replaceChildren ();

  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      let id = map[y][x];
      let tile = get_tile (id);

      display_tile (y, x, tile.src, id);
    }
  }
}

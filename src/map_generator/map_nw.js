function map_nw_get_options_for_xy (screen, x, y) {
  let options;

  if (x == 0 && y == 0) {
    options = Object.keys (neighbours);
  } else if (y == 0) {
    let left = screen[0][x-1];
    options = neighbours[left].right;
  } else if (x == 0) {
    let up = screen[y-1][0];
    options = neighbours[up].down;
  } else {
    let left = screen[y][x-1];
    let options_lr = neighbours[left].right;

    let up = screen[y-1][x];
    let options_ud = neighbours[up].down;

    options = [];
    for (let lr of options_lr) {
      for (let ud of options_ud) {
        if (lr == ud) {
          options.push (lr);
        }
      }
    }
  }

  return options;
}

function map_nw_generate (tiles_wide, tiles_high) {
  const screen = [];
  let uh_ohs = 0;

  outer:
  for (let y = 0; y < tiles_high; y++) {
    let row = [];
    screen.push (row);

    for (let x = 0; x < tiles_wide; x++) {
      let id = 0;

      let options = map_nw_get_options_for_xy (screen, x, y);

      if (options.length == 0) {
        uh_ohs++;

        if (uh_ohs > 1000) {
          break outer;
        }
        console.log ("UH OH!");

        /* re-do the last one */
        if (x > 0) {
          x -= 2;
          continue;
        } else {
          x = 8;
          y -= 1;
          row = screen[y];
          continue;
        }
      }

      id = list_get_rand (options);

      if (!id) {
        console.log (screen);
        throw new Exception ("WOW");
      }

      if (x < row.length) {
        row[x] = id;
      } else {
        row.push (id);
      }
    }
  }

  return screen;
}

/*** squared frequency percentage table ***/

function freq_new (array) {
  let freq = {};
  array.map (x => { freq[x] = (freq[x] ?? 0.0) + 1 });

  let square_sum = Object.keys (freq).reduce ((ss,x) => ss += freq[x]*freq[x], 0);
  let square_perc = {};
  Object.keys (freq).map (x => { square_perc[x] = (freq[x]*freq[x]) / square_sum });

  return square_perc;
}

function freq_remove (freq, key) {
  console.log (`before: freq `, freq, `key ${key}`);
  let removed_freq = freq[key];
  delete (freq[key]);
  for (let f in freq) {
    freq[f] = freq[f] / (1.0 - removed_freq);
  }
  console.log (` after: freq `, freq, `key ${key}`);
}

function freq_get_rand (freq) {
  const r = Math.random ();
  let sum = 0.0;

  for (const x in freq) {
    sum += freq[x];
    if (r < sum + 0.00001) {
      return x;
    }
  }
}

/*** map_c ***/

function map_c_get_xy (screen, x, y) {
  if (screen[y]) {
    if (screen[y][x]) {
      return screen[y][x];
    }
  }
  return -1;
}

/* Possibly obsolete as we have since explicitly added up and left to our relationships,
   as the result of our reverse counting did not give us the distribution we actually wanted.
  */
function map_c_get_neighbours_reverse (target_id, dir /* down or right */, opposite_dir) {
  if (!neighbours[target_id][opposite_dir]) {
    let options = [];

    for (let id in neighbours) {
      let id_num = parseInt (id);
      let num_matches = neighbours[id][dir].filter (x => x == target_id).length;
      for (let i = 0; i < num_matches; i++) {
        options.push (id_num);
      }
    }

    neighbours[target_id][opposite_dir] = options;
  }

  return neighbours[target_id][opposite_dir];
}

function map_c_get_options_for_xy (screen, x, y) {
  console.log (`--------------------`);
  console.log (`x,y == ${x},${y}`);

  let option_sets = [];
  let id = -1;

  if ((id = map_c_get_xy (screen, x, y+1)) > -1) {
    /* south of us; we're up */
    console.log (`x,y ${x},${y} south neighbour tile ID ${id} has .up of:`, neighbours[id].up);
    display_screen_text (screen);
    option_sets.push (neighbours[id].up);
  }
  if ((id = map_c_get_xy (screen, x+1, y)) > -1) {
    /* east of us; we're left */
    console.log (`x,y ${x},${y} east neighbour tile ID ${id} has .left of: `, neighbours[id].left);
    option_sets.push (neighbours[id].left);
  }
  if ((id = map_c_get_xy (screen, x, y-1)) > -1) {
    /* north of us; we're down */
    console.log (`x,y ${x},${y} north neighbour tile ID ${id} has .down of: `, neighbours[id].down);
    option_sets.push (neighbours[id].down);
  }
  if ((id = map_c_get_xy (screen, x-1, y)) > -1) {
    /* west of us; we're right */
    console.log (`x,y ${x},${y} west neighbour tile ID ${id} has .right of: `, neighbours[id].right);
    option_sets.push (neighbours[id].right);
  }

  if (option_sets.length == 0) {
    return { 147: 1.0 };
  }

  /* reduce to the interaction across sets */
  let total_options = [ ...(option_sets[0] ?? [147] ) ];
  for (let o = 1; o < option_sets.length; o++) {
    total_options = total_options.filter (x => option_sets[o].includes (x));
  }
  if (total_options.length == 0) {
    return false;
  }

  for (let o = 0; o < option_sets.length; o++) {
    console.log (`option_sets.length ${option_sets.length} o ${o} option_sets`, option_sets);
    option_sets[o] = option_sets[o].filter (x => total_options.includes (x));
  }

  console.log (`total_options`, total_options);

  /* calc frqeuencies of reduced options */
  const total_freq = {};
  for (let options of option_sets) {
    const freq = freq_new (options);
    console.log (`freq`, freq, `option_sets.length`, option_sets.length);

    for (let tile_id of Object.keys (freq)) {
      total_freq[tile_id] = (total_freq[tile_id] ?? 0.0) + freq[tile_id] / option_sets.length;
    }
  }

  console.log (total_freq, option_sets);
  console.log (`sum: `,Object.values (total_freq).reduce ( (a,v) => a + v, 0));

  return total_freq;
}

let g_uh_ohs = 0;
function map_c_next (screen, x, y, c, ring_length, d, dirs, i, total_tiles) {
  if (i == total_tiles) {
    /* we filled them map! */
    return true;
  }

  let freq = map_c_get_options_for_xy (screen, x, y, dirs[d]);
  if (!freq) {
    g_uh_ohs++;
    return false;
  }

  /* params for next step in mapping */
  const new_c = (c + 1) % ring_length; /* advance in our line */
  const new_d = (d + (new_c == 0 ? 1 : 0)) % dirs.length; /* possibly change direction if starting new line */
  const new_ring_length = ring_length + ((new_c == 0 && new_d % 2 == 0) ? 1 : 0); /* extend length if n or w */
  const new_x = x + dirs[d].x;
  const new_y = y + dirs[d].y;
  const new_i = i + 1;

  /* try all of our options, abandoning the first viable */
  while (Object.keys (freq).length > 0) {
    const tile_id = freq_get_rand (freq);
    screen[y][x] = tile_id;
    console.log (`x,y ${x},${y} ${i}/${total_tiles} tile_id ${tile_id} freq`, freq);

    if (map_c_next (screen, new_x, new_y,
                      new_c, new_ring_length,
                      new_d, dirs,
                      new_i, total_tiles)) {

      return true; /* subsequent tiles were successful */
    } else {
      if (g_uh_ohs > 100000) {
        return true; /* bail */
      }


      /* uh oh, we hit a dead-end later, we'll have to try something else */
      console.log (`x,y ${x},${y} removing unviable ${tile_id}`);
      freq_remove (freq, tile_id);
      if (Object.keys (freq).length == 0) {
        break;
      }
    }
  }

  /* none of our options lead to a viable outcome, backtrack further */
  screen[y][x] = -1;
  g_uh_ohs++;

  return false;
}

function map_c_generate (tiles_wide, tiles_high) {
  if (tiles_wide != tiles_high || tiles_wide % 2 == 0 || tiles_high % 2 == 0) {
    window.alert ("map_c requires an equal, odd number of tiles for both height and width.  E.g. 9x9")
    return;
  }

  const screen = [];
  for (let y = 0; y < tiles_high; y++) {
    let row = [];
    for (let x = 0; x < tiles_wide; x++) {
      row.push (-1);
    }
    screen.push (row);
  }

  const center_y = Math.floor (tiles_high / 2);
  const center_x = Math.floor (tiles_wide / 2);

  /* s > e > n >  w */
  /* s1 e1 n2 w2 s3 e3 n4 w4 s5 e5 */
  const total_tiles = tiles_high * tiles_wide;
  const dirs = [ {x:0,y:1}, {x:1,y:0}, {x:0,y:-1}, {x:-1,y:0} ]; /* [x,y] pairs */

  let ring_length = 0;
  let y = center_y;
  let x = center_x;

  map_c_next (screen, x, y, 0, 1, 0, dirs, 0, total_tiles);

  return screen;
}

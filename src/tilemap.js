/* tilemap.js:
 *
 * Provides a mapping of tile ID to tile definitions.  (Not a grid map.)
 *
 * == Tile ID and Tile Group ID ==
 *
 * Tiles have a unique *Tile ID*, and also often belong to thematic groups that have a separate
 * *Tile Group ID* that corresponds to their directory organization.
 *
 * E.g. for red brick wall textures, PNGs can be found in:
 *     3.house/2.walls/1.tx/2.walls/1.red brick/
 *
 *   This tile group has the numeric identifier of
 *     32121,
 *
 *   which is prefixed to individual tiles.  So within "1.red brick/", you would find:
 *     filename         tile ID
 *     ------------------------
 *     32121.sw.png     117
 *     32121.s.png      118
 *     32121.se.png     119
 *     etc.
 *
 * Each of those tile files are mapped to a Tile ID to be used in map grids.
 *
 * Looking at the Tile Group ID in a file name, you can relate individual digits
 * to the kind of tile (e.g. starting with 32 puts it in the house (3) walls (2) group).
 * Meanwhile, the unique Tile ID for each tile is not even gauranteed to be
 * sequential within a group of related tiles.  (e.g. if some tiles are added
 * later and assigned a much higher ID than those they belong to.)
 *
 * == Properties of a tile definition ==
 * As of 2023-04-21:
 *
 *   Required:
 *     go: <bool>             true if a unit can walk on the tile, false if not
 *
 *     src: <string>          The path in the tileset to a given tile PNG image.
 *     | src: <array>         Combination of tiles, layered from lowest (i=0) to highest as i increases.
 *     | rand_set: <array>    A list of other tile IDs, one of which will be selected and used in place of this tile.
 *
 *   Optional:
 *     z: <int>           z-index for the tile on the map, to override the default.  Defaults: if go: true, then z: 0 (below the player unit)
 *                        If go: false, then z: 2 (above the player unit)
 *     battle: <float>    Chance of encountering a battle on said tile. (TODO separate this from the tile)
 *     teleport:          If entering this tile should teleport the player unit (e.g. a doorway).
 *     bump:              If the unit player can interact directly with it. (E.g. a sign pops up information)
 *     filter:            Apply a filter function to the appearance of the tile, possibility dependent on an RNG or a tile's position
 */
const filters = {
  "alpha": function (start_pos) {
    hue = (start_pos.y + start_pos.x * 7) % 360;
    return `sepia(100%) hue-rotate(${hue}deg) saturate(1)`;
  },
};

const tile_map = {
  0: {  go: false },
  1: {  go: true  },
  2: {  go: true,  battle: 1.0 },
  3: {  go: true,  battle: 0.1 },

  /* ==== Outdoors ==== */

  /* == 10: Grass == */
  4: {  go: true,  src: "1.nature/0.grasses/10.1.png" },
  5: {  go: true,  src: "1.nature/0.grasses/10.2.png" },
  6: {  go: true,  src: "1.nature/0.grasses/10.3.png" },
  43: { go: true,  src: "1.nature/0.grasses/10.4.png" },
  66: { go: true,  src: "1.nature/0.grasses/10.5.png" },
  67: { go: true,  src: "1.nature/0.grasses/10.6.png" },
  68: { go: true,  src: "1.nature/0.grasses/10.7.png" },
  69: { go: true,  src: "1.nature/0.grasses/10.8.png" },
  19: { go: true,  rand_set: [ 4, 5, 6, 6, 43, 43, 43, 43, 66, 66, 67, 68, 69 ] }, /* grass mix */

  147: { go: true, rand_set: [ 19, 19, 19, 31, 159, 162, 163 ] }, /* nature, go */

  /* == 12: Rock == */
  159: { go: true,  src: "1.nature/2.rocks/12.1.png" }, /* small rocks, walkable */
  160: { go: false, src: "1.nature/2.rocks/12.2.png" }, /* big rock 1 */
  161: { go: false, src: "1.nature/2.rocks/12.3.png" }, /* big rock 2 */
  162: { go: true,  src: [ 159, 19 ] },                 /* waklable grass+stones */
  163: { go: true,  rand_set: [ 159, 162 ] },           /* rock mix: go */
  164: { go: false, rand_set: [ 160, 161 ] },           /* rock mix: no-go */

  /* == 13: Water == */
  148: { go: true, src: "1.nature/3.water/13.sw.png" },
  149: { go: true, src: "1.nature/3.water/13.s.png" },
  150: { go: true, src: "1.nature/3.water/13.se.png" },
  151: { go: true, src: "1.nature/3.water/13.e.png" },
  152: { go: true, src: "1.nature/3.water/13.ne.png" },
  153: { go: true, src: "1.nature/3.water/13.n.png" },
  154: { go: true, src: "1.nature/3.water/13.nw.png" },
  155: { go: true, src: "1.nature/3.water/13.w.png" },

  173: { go: true, src: "1.nature/3.water/13.sw in.png" },
  174: { go: true, src: "1.nature/3.water/13.se in.png" },
  175: { go: true, src: "1.nature/3.water/13.ne in.png" },
  176: { go: true, src: "1.nature/3.water/13.nw in.png" },

  156: { go: false, src: "1.nature/3.water/13.c1.png"  }, /* TODO: do layered ones, where we look like we're half in */
  157: { go: false, src: "1.nature/3.water/13.c2.png"  },
  158: { go: false, rand_set: [ 156, 157 ] },             /* water mix */

  /* == 15: Ridges/Shallow Cliffs == */

  /*   150: ends */
  256: { go: false, src: "1.nature/5.ridge/0.ends/150.e.png", z: 0  },
  257: { go: false, src: "1.nature/5.ridge/0.ends/150.w.png", z: 0  },
  /*   151: lines */
  258: { go: false, src: "1.nature/5.ridge/1.lines/151.h.n.png", z: 0  },
  259: { go: false, src: "1.nature/5.ridge/1.lines/151.h.s.png", z: 0  },
  260: { go: false, src: "1.nature/5.ridge/1.lines/151.v.e.png", z: 0  },
  261: { go: false, src: "1.nature/5.ridge/1.lines/151.v.w.png", z: 0  },
  /*   152: corners outside */
  262: { go: false, src: "1.nature/5.ridge/2.corners outside/152.ne.png", z: 0  },
  263: { go: false, src: "1.nature/5.ridge/2.corners outside/152.nw.png", z: 0  },
  264: { go: false, src: "1.nature/5.ridge/2.corners outside/152.se.png", z: 0  },
  265: { go: false, src: "1.nature/5.ridge/2.corners outside/152.sw.png", z: 0  },
  /*   153: corners outside */
  266: { go: false, src: "1.nature/5.ridge/3.corners inside/153.ne.in.png", z: 0  },
  267: { go: false, src: "1.nature/5.ridge/3.corners inside/153.nw.in.png", z: 0  },
  268: { go: false, src: "1.nature/5.ridge/3.corners inside/153.se.in.png", z: 0  },
  269: { go: false, src: "1.nature/5.ridge/3.corners inside/153.sw.in.png", z: 0  },

  /* == Trees == */
  270: { go: false, src: "1.nature/4.trees/1.bushes/c.bush.png", z: 0  },

  271: { go: false, src: "1.nature/4.trees/0.trees/s.trunk.png", z: 0  },
  272: { go: false, src: "1.nature/4.trees/0.trees/n.tree deciduous round.png"  },
  273: { go: false, src: "1.nature/4.trees/0.trees/c1.tree deciduous round.png"  },
  274: { go: false, src: "1.nature/4.trees/0.trees/n1.tree deciduous round.png"  },



  /* == Path == */

  /* 221: Dirt Path */

  /*   ends */
  165: { go: true, src: "2.outside/2.paths/1.dirt path/221.s.png" },
  166: { go: true, src: "2.outside/2.paths/1.dirt path/221.e.png" },
  167: { go: true, src: "2.outside/2.paths/1.dirt path/221.n.png" },
  168: { go: true, src: "2.outside/2.paths/1.dirt path/221.w.png" },

  /*   lines - | */
  169: { go: true, src: "2.outside/2.paths/1.dirt path/221.v.png" },
  170: { go: true, src: "2.outside/2.paths/1.dirt path/221.h.png" },

  /*   4-way + */
  171: { go: true, src: "2.outside/2.paths/1.dirt path/221.c.png" },

  /*   elbows L */
  177: { go: true, src: "2.outside/2.paths/1.dirt path/221.s-w.png" },
  178: { go: true, src: "2.outside/2.paths/1.dirt path/221.s-e.png" },
  179: { go: true, src: "2.outside/2.paths/1.dirt path/221.n-e.png" },
  180: { go: true, src: "2.outside/2.paths/1.dirt path/221.n-w.png" },

  /*   3-way T */
  181: { go: true, src: "2.outside/2.paths/1.dirt path/221.n-w-s.png" },
  182: { go: true, src: "2.outside/2.paths/1.dirt path/221.w-s-e.png" },
  183: { go: true, src: "2.outside/2.paths/1.dirt path/221.n-e-s.png" },
  184: { go: true, src: "2.outside/2.paths/1.dirt path/221.w-n-e.png" },


  /* ==== House === */

  /* == House (outside) == */

  /* fr+tx sets */
  /*   red brick */
  7: {  go: false, src: [ 117, 112 ], z: 0 },  /* sw */
  8: {  go: false, src: [ 118, 113 ], z: 0 },  /* s */
  9: {  go: true,  src: [   8, 140 ], z: 0, teleport: true }, /* s + door */
  10: { go: false, src: [   8, 139 ], z: 0 },  /* s + window */
  11: { go: false, src: [ 119, 114 ], z: 0 },  /* se */
  12: { go: false, src: [ 120, 115 ] },        /* w */
  13: { go: false, src: [      121 ] },        /* c */
  15: { go: false, src: [ 122, 116 ] },        /* e */

  /*   red brick + window (2nd story) */
  14: { go: false, src: [ 13, 135 ] }, /* c + window1 */
  44: { go: false, src: [ 13, 136 ] }, /* c + window2 */
  45: { go: false, src: [ 13, 137 ] }, /* c + window3 */
  46: { go: false, src: [ 13, 138 ] }, /* c + window4 */

  /*   white panel */
  47: { go: false, src: [ 129, 112 ], z: 0 }, /* sw */

  142: { go: false, src: [ 130, 113 ], z: 0 },  /* s */
  143: { go: true,  src: [ 142, 141 ], z: 0, teleport: true }, /* s + door */
  144: { go: false, src: [ 142, 139 ], z: 0 }, /* s + window */
  246: { go: false, src: [ 142, 247 ], z: 0 }, /* s + ❤ sign */

  48: { go: false, src: [ 131, 114 ], z: 0 }, /* se */

  49: { go: false, src: [ 132, 115 ] }, /* w */
  50: { go: false, src: [ 134, 116 ] }, /* e */


  /*   white panel + window (2nd story) */
  51: { go: false, src: [ 133, 135 ] }, /* c_window1 */
  52: { go: false, src: [ 133, 136 ] }, /* c_window2 */
  53: { go: false, src: [ 133, 137 ] }, /* c_window3 */
  54: { go: false, src: [ 133, 138 ] }, /* c_window4 */


  /* frame */
  112: { go: false, src: "3.house/2.walls/0.fr/0/3200.sw.png", z: 0 },
  113: { go: false, src: "3.house/2.walls/0.fr/0/3200.s.png",  z: 0 },
  114: { go: false, src: "3.house/2.walls/0.fr/0/3200.se.png", z: 0 },
  115: { go: false, src: "3.house/2.walls/0.fr/0/3200.w.png" },
  116: { go: false, src: "3.house/2.walls/0.fr/0/3200.e.png" },

  /* textures */
  /*   red brick */
  117: { go: false, src: "3.house/2.walls/1.tx/2.walls/1.red brick/32121.sw.png", z: 0 },
  118: { go: false, src: "3.house/2.walls/1.tx/2.walls/1.red brick/32121.s.png",  z: 0 },
  119: { go: false, src: "3.house/2.walls/1.tx/2.walls/1.red brick/32121.se.png", z: 0 },
  120: { go: false, src: "3.house/2.walls/1.tx/2.walls/1.red brick/32121.w.png" },
  121: { go: false, src: "3.house/2.walls/1.tx/2.walls/1.red brick/32121.c.png" },
  122: { go: false, src: "3.house/2.walls/1.tx/2.walls/1.red brick/32121.e.png" },

  /*   brown */
  123: { go: false, src: "3.house/2.walls/1.tx/2.walls/2.brown/32122.sw.png", z: 0 },
  124: { go: false, src: "3.house/2.walls/1.tx/2.walls/2.brown/32122.s.png",  z: 0 },
  125: { go: false, src: "3.house/2.walls/1.tx/2.walls/2.brown/32122.se.png", z: 0 },
  126: { go: false, src: "3.house/2.walls/1.tx/2.walls/2.brown/32122.w.png" },
  127: { go: false, src: "3.house/2.walls/1.tx/2.walls/2.brown/32122.c.png" },
  128: { go: false, src: "3.house/2.walls/1.tx/2.walls/2.brown/32122.e.png" },

  /*   white panel */
  129: { go: false, src: "3.house/2.walls/1.tx/2.walls/3.white panel/32123.sw.png", z: 0 },
  130: { go: false, src: "3.house/2.walls/1.tx/2.walls/3.white panel/32123.s.png",  z: 0 },
  131: { go: false, src: "3.house/2.walls/1.tx/2.walls/3.white panel/32123.se.png", z: 0 },
  132: { go: false, src: "3.house/2.walls/1.tx/2.walls/3.white panel/32123.w.png" },
  133: { go: false, src: "3.house/2.walls/1.tx/2.walls/3.white panel/32123.c.png" },
  134: { go: false, src: "3.house/2.walls/1.tx/2.walls/3.white panel/32123.e.png" },

  /* window (no wall) */
  135: { go: false, src: "3.house/1.windows/31.c_1.png" }, /* for 2nd floor */
  136: { go: false, src: "3.house/1.windows/31.c_2.png" },
  137: { go: false, src: "3.house/1.windows/31.c_3.png" },
  138: { go: false, src: "3.house/1.windows/31.c_4.png" },

  139: { go: false, src: "3.house/1.windows/31.s_1.png" }, /* for first floor */

  /* 30: doors */
  140: { go: true,  src: "3.house/0.doors/30.0.png" }, /* wood */
  141: { go: true,  src: "3.house/0.doors/30.1.png" }, /* glass */

  /* 34: decorations */
  247: { go: true,  src: "3.house/4.decorations/34.0.health heart.png" }, /* ❤ */

  /* == House Roof == */

  /* single roof */
  /*   fr+tx */
  /*     Green Metal (+red brick wall) */
  16: { go: false, src: [ 88, 106, 85 ], }, /* sw */
  17: { go: false, src: [ 121, 86, 89 ], }, /* s  */
  18: { go: false, src: [ 107, 87, 90 ], }, /* se */
  /* 19: with grasses */

  /*     Red Thatch */
  82: { go: false, src: [ 85, 91 ], }, /* sw */
  83: { go: false, src: [ 86, 92 ], }, /* s  */
  84: { go: false, src: [ 87, 93 ], }, /* se */

  /*   frame */
  85: { go: false, src: "3.house/3.roofs/0.fr/0.single/0/33000.nw,w,sw.png" },
  86: { go: false, src: "3.house/3.roofs/0.fr/0.single/0/33000.n.png"       },
  87: { go: false, src: "3.house/3.roofs/0.fr/0.single/0/33000.se,e,ne.png" },

  /*   textures */
  /*     Green Metal */
  88: { go: false, src: "3.house/3.roofs/1.tx/0.single/1.green metal/33101.nw_w_sw.png" },
  89: { go: false, src: "3.house/3.roofs/1.tx/0.single/1.green metal/33101.n.png"       },
  90: { go: false, src: "3.house/3.roofs/1.tx/0.single/1.green metal/33101.se_e_ne.png" },

  /*     Red Thatch */
  91: { go: false, src: "3.house/3.roofs/1.tx/0.single/2.red thatch/33102.nw_w_sw.png" },
  92: { go: false, src: "3.house/3.roofs/1.tx/0.single/2.red thatch/33102.n.png" },
  93: { go: false, src: "3.house/3.roofs/1.tx/0.single/2.red thatch/33102.se_e_ne.png" },


  /* double roof */
  /*   Green Metal fr+tx */
  20:  { go: false, src: [ 145, 94, 100 ] }, /* sw + white panel */
  21:  { go: false, src: [ 133, 95, 101 ] }, /* s + white panel  */
  22:  { go: false, src: [ 146, 96, 102 ] }, /* se + white panel */

  110: { go: false, src: [ 94, 100, 108 ] }, /* sw + brown */
  // 21:  { go: false, src: [ 95, 101 ] },      /* s + brown */
  111: { go: false, src: [ 96, 102, 109 ] }, /* se + brown */

  // 110: { go: false, src: [ 94, 100, 108 ] }, /* sw + redbrick */
  // 21:  { go: false, src: [ 95, 101 ] },      /* s + redbrick  */
  // 111: { go: false, src: [ 96, 102, 109 ] }, /* se + redbrick */


  23: { go: true,  src: [ 97, 103 ], z: 2 }, /* nw */
  24: { go: true,  src: [ 98, 104 ], z: 2 }, /* n */
  25: { go: true,  src: [ 99, 105 ], z: 2 }, /* ne */

  77: { go: true,  src: [ 19, 23 ] }, /* nw roof + grass */
  78: { go: true,  src: [ 19, 24 ] }, /*  n roof + grass */
  79: { go: true,  src: [ 19, 25 ] }, /* ne roof + grass */

  /*   33010: frames */
  94: { go: false, src: "3.house/3.roofs/0.fr/1.double/0/33010.sw.png" },
  95: { go: false, src: "3.house/3.roofs/0.fr/1.double/0/33010.s.png"  },
  96: { go: false, src: "3.house/3.roofs/0.fr/1.double/0/33010.se.png" },
  97: { go: true,  src: "3.house/3.roofs/0.fr/1.double/0/33010.nw.png", z: 2 },
  98: { go: true,  src: "3.house/3.roofs/0.fr/1.double/0/33010.n.png",  z: 2 },
  99: { go: true,  src: "3.house/3.roofs/0.fr/1.double/0/33010.ne.png", z: 2 },

  /*   textures */
  /*     33111: Green Metal */
  100: { go: false, src: "3.house/3.roofs/1.tx/1.double/1.green metal/33111.sw.png" },
  101: { go: false, src: "3.house/3.roofs/1.tx/1.double/1.green metal/33111.s.png" },
  102: { go: false, src: "3.house/3.roofs/1.tx/1.double/1.green metal/33111.se.png" },
  103: { go: true,  src: "3.house/3.roofs/1.tx/1.double/1.green metal/33111.nw.png", z: 2 },
  104: { go: true,  src: "3.house/3.roofs/1.tx/1.double/1.green metal/33111.n.png",  z: 2 },
  105: { go: true,  src: "3.house/3.roofs/1.tx/1.double/1.green metal/33111.ne.png", z: 2 },

  /* == House Wall+Roof == */

  /* 32111: Red Brick */
  106: { go: false,  src: "3.house/2.walls/1.tx/1.roof double/1.red brick/32111.sw.png", z: 2 },
  107: { go: false,  src: "3.house/2.walls/1.tx/1.roof double/1.red brick/32111.se.png", z: 2 },

  /* 32112: Brown */
  108: { go: false,  src: "3.house/2.walls/1.tx/1.roof double/2.brown/32112.sw.png", },
  109: { go: false,  src: "3.house/2.walls/1.tx/1.roof double/2.brown/32112.se.png", },

  /* 32113: White Panel */
  145: { go: false,  src: "3.house/2.walls/1.tx/1.roof double/3.white panel/32113.sw.png", },
  146: { go: false,  src: "3.house/2.walls/1.tx/1.roof double/3.white panel/32113.se.png", },



  /* 26 down with fences, 27- */

  /* == 11: Tall grass (battle tiles) == */
  27: {  go: true,  src: "1.nature/1.tall grasses/11.1.png", battle: 0.025 },
  28: {  go: true,  src: "1.nature/1.tall grasses/11.2.png", battle: 0.05 },
  29: {  go: true,  src: "1.nature/1.tall grasses/11.3.png", battle: 0.10 },
  30: {  go: true,  src: "1.nature/1.tall grasses/11.4.png", battle: 0.15 },
  31: {  go: true,  rand_set: [ 27, 27, 27, 28, 28, 28, 29, 29, 30 ], },
  32: {  go: true,  src: "1.nature/1.tall grasses/11.1.png", battle: 1.0 },

  /* == 211, 201: Fencing and signage == */
  26: { go: false, src: "2.outside/1.signs/1.wood/211.c.png",   z: 0, bump: true, },
  33: { go: false, src: "2.outside/0.fences/1.wood/201.w.png",  z: 0 },
  34: { go: false, src: "2.outside/0.fences/1.wood/201.ch.png", z: 0 },
  35: { go: false, src: "2.outside/0.fences/1.wood/201.e.png",  z: 0 },

  36: { go: false, src: "2.outside/0.fences/1.wood/201.n.png",  z: 0 },
  37: { go: false, src: "2.outside/0.fences/1.wood/201.cv.png", z: 0 },
  38: { go: false, src: "2.outside/0.fences/1.wood/201.s.png",  z: 0 },

  39: { go: false, src: "2.outside/0.fences/1.wood/201.nw.png", z: 0 },
  40: { go: false, src: "2.outside/0.fences/1.wood/201.sw.png", z: 0 },
  41: { go: false, src: "2.outside/0.fences/1.wood/201.se.png", z: 0 },
  42: { go: false, src: "2.outside/0.fences/1.wood/201.ne.png", z: 0 },

  /* 43 is with grasses */
  /* 44-54 are with house windows */

  /* == Indoors == */
  55: { go: false, src: "4.inside/4.9.nullspace.png", z: 2 },

  /* 41: for blank floor, consider using tile 1 */
  56: { go: false, src: "4.inside/1.walls/41.nw.png", z: 0, filter: 'alpha' },
  57: { go: true,  src: "4.inside/1.walls/41.w.png",  },
  58: { go: true,  src: "4.inside/1.walls/41.sw.png", z: 2 },
  59: { go: true,  src: "4.inside/1.walls/41.s.png",  z: 2 },
  60: { go: true,  src: "4.inside/1.walls/41.se.png", z: 2 },
  61: { go: true,  src: "4.inside/1.walls/41.e.png",  },
  62: { go: false, src: "4.inside/1.walls/41.ne.png", z: 0, filter: 'alpha' },
  63: { go: false, src: "4.inside/1.walls/41.n.png",  z: 0, filter: 'alpha' },

  64: { go: true,  src: "4.inside/1.walls/41.s_door.png",     z: 2, teleport: true, },
  65: { go: false, src: [ 63, 81 ],                           bump: true, },
  // X 80: { go: false, src: "inside/inside_n_wall_painting.png", z: 0, bump: view_painting_bump_func },

  /* 42: Objects */
  81: { go: false, src: "4.inside/2.objects/42.painting.png",         z: 0, bump: true, },
  172: { go: false, src: "4.inside/2.objects/42.vending machine.png", z: 0, bump: true, },

  248: { go: false, src: "4.inside/3.furniture/00.chairs/1.cherry oak/43001.s.png", },
  249: { go: false, src: "4.inside/3.furniture/00.chairs/1.cherry oak/43001.e.png", },
  250: { go: false, src: "4.inside/3.furniture/00.chairs/1.cherry oak/43001.n.png", },
  251: { go: false, src: "4.inside/3.furniture/00.chairs/1.cherry oak/43001.w.png", },
  252: { go: false, src: "4.inside/3.furniture/01.tables/1.cherry oak/43011.c.png", },
  253: { go: false, src: "4.inside/3.furniture/02.counter/1.cherry oak/43021.s.png", z: 0 },
  254: { go: false, src: "4.inside/3.furniture/02.counter/1.cherry oak/43021.c.png", },
  255: { go: true,  src: "4.inside/3.furniture/02.counter/1.cherry oak/43021.n.png", z: 2 },


  /* 66-69 with grasses */

  /* 401: tile flooring */
  70: { go: true, src: "4.inside/0.floors/1.grey tile/401.c.png", filter: 'alpha' },
  71: { go: true, src: [ 70, 57 ], }, /* w  */
  72: { go: true, src: [ 70, 58 ], }, /* sw */
  73: { go: true, src: [ 70, 59 ], }, /* s  */
  74: { go: true, src: [ 70, 60 ], }, /* se */
  75: { go: true, src: [ 70, 61 ], }, /* e  */
  76: { go: true, src: [ 70, 64 ], teleport: true }, /* door */

  275: { go: true, src: "4.inside/0.floors/2.wood plank v/402.0.png", },
  276: { go: true, src: "4.inside/0.floors/2.wood plank v/402.1.png", },
  277: { go: true, src: "4.inside/0.floors/2.wood plank v/402.2.png", },
  278: { go: true, rand_set: [ 275, 276, 277 ], },


  /* 77-79 with Roofs */
  /* 80-81 with Indoors */

  /* 222: Sidewalk */
  185: { go: true, src: "2.outside/2.paths/2.side walk/0.lines and caps/2220.h.png", },
  186: { go: true, src: "2.outside/2.paths/2.side walk/0.lines and caps/2220.e.png", },
  187: { go: true, src: "2.outside/2.paths/2.side walk/0.lines and caps/2220.n.png", },
  188: { go: true, src: "2.outside/2.paths/2.side walk/0.lines and caps/2220.s.png", },
  189: { go: true, src: "2.outside/2.paths/2.side walk/0.lines and caps/2220.v.png", },
  190: { go: true, src: "2.outside/2.paths/2.side walk/0.lines and caps/2220.w.png", },
  191: { go: true, src: "2.outside/2.paths/2.side walk/1.2w corners closed/2221.ne.2c(c:s-w).png", },
  192: { go: true, src: "2.outside/2.paths/2.side walk/1.2w corners closed/2221.nw.2c(c:s-e).png", },
  193: { go: true, src: "2.outside/2.paths/2.side walk/1.2w corners closed/2221.se.2c(c:n-w).png", },
  194: { go: true, src: "2.outside/2.paths/2.side walk/1.2w corners closed/2221.sw.2c(c:n-e).png", },
  195: { go: true, src: "2.outside/2.paths/2.side walk/2.2w corners open/2222.ne.2o.png", },
  196: { go: true, src: "2.outside/2.paths/2.side walk/2.2w corners open/2222.nw.2o.png", },
  197: { go: true, src: "2.outside/2.paths/2.side walk/2.2w corners open/2222.se.2o.png", },
  198: { go: true, src: "2.outside/2.paths/2.side walk/2.2w corners open/2222.sw.2o.png", },
  199: { go: true, src: "2.outside/2.paths/2.side walk/3.3w corners closed/2223.e.3c(c:n-w-s).png", },
  200: { go: true, src: "2.outside/2.paths/2.side walk/3.3w corners closed/2223.n.3c(c:w-s-e).png", },
  201: { go: true, src: "2.outside/2.paths/2.side walk/3.3w corners closed/2223.s.3c(c:w-n-e).png", },
  202: { go: true, src: "2.outside/2.paths/2.side walk/3.3w corners closed/2223.w.3c(c:n-e-s).png", },
  203: { go: true, src: "2.outside/2.paths/2.side walk/4.3w corners open/2224.e.3o(c:n-w-s).png", },
  204: { go: true, src: "2.outside/2.paths/2.side walk/4.3w corners open/2224.n.3o(c:w-s-e).png", },
  205: { go: true, src: "2.outside/2.paths/2.side walk/4.3w corners open/2224.s.3o(c:w-n-e).png", },
  206: { go: true, src: "2.outside/2.paths/2.side walk/4.3w corners open/2224.w.3o(c:n-e-s).png", },
  207: { go: true, src: "2.outside/2.paths/2.side walk/4.4w closed and open/2224.c.open.png", },
  208: { go: true, src: "2.outside/2.paths/2.side walk/4.4w closed and open/2224.c.closed.png", },

  /*** 223: ROAD ***/

  /* pavement (texture) */
  209: { go: true, src: "2.outside/2.paths/3.road/c.png", },


  /** Lines **/

  /* 223000: single solid dividers */
  210: { go: true, src: "2.outside/2.paths/3.road/0.lines/0.straight/0.solid/223000.n.(l:w).png", },
  211: { go: true, src: "2.outside/2.paths/3.road/0.lines/0.straight/0.solid/223000.s.(l:e).png", },

  212: { go: true, src: "2.outside/2.paths/3.road/0.lines/0.straight/0.solid/223000.w.(l:s).png", },
  213: { go: true, src: "2.outside/2.paths/3.road/0.lines/0.straight/0.solid/223000.e.(l:n).png", },

  /*     double parallel solid dividers, for traffic or cross walks */
  214: { go: true, src: "2.outside/2.paths/3.road/0.lines/0.straight/0.solid/223000.v.(l:w-e).png", },
  215: { go: true, src: "2.outside/2.paths/3.road/0.lines/0.straight/0.solid/223000.h.(l:n-s).png", },

  /* 2230: corners, solid: 2-side divider (parallel) + road */

  /*   22301: inside corner L */
  216: { go: true, src: "2.outside/2.paths/3.road/0.lines/1.turns inside/22301.1.e-s.(l:n-e).png", },
  217: { go: true, src: "2.outside/2.paths/3.road/0.lines/1.turns inside/22301.2.s-w.(l:s-e).png", },
  218: { go: true, src: "2.outside/2.paths/3.road/0.lines/1.turns inside/22301.3.w-n.(l:s-w).png", },
  219: { go: true, src: "2.outside/2.paths/3.road/0.lines/1.turns inside/22301.4.n-e.(l:n-w).png", },

  /*   22302: outside  corner ` */
  220: { go: true, src: "2.outside/2.paths/3.road/0.lines/2.turns outside/22302.1.n-w.(l:s-w).png", },
  221: { go: true, src: "2.outside/2.paths/3.road/0.lines/2.turns outside/22302.2.w-s.(l:s-e).png", },
  222: { go: true, src: "2.outside/2.paths/3.road/0.lines/2.turns outside/22302.3.s-e.(l:n-e).png", },
  223: { go: true, src: "2.outside/2.paths/3.road/0.lines/2.turns outside/22302.4.e-n.(l:n-w).png", },

  /*   22303: thick stop line for intersections */
  224: { go: true, src: "2.outside/2.paths/3.road/0.lines/3.+ stop line/22303.s.png", },
  225: { go: true, src: "2.outside/2.paths/3.road/0.lines/3.+ stop line/22303.e.png", },
  226: { go: true, src: "2.outside/2.paths/3.road/0.lines/3.+ stop line/22303.n.png", },
  227: { go: true, src: "2.outside/2.paths/3.road/0.lines/3.+ stop line/22303.w.png", },


  /** Lines + Pavement ***/
  /* 223000: single solid dividers */
  228: { go: true, src: [ 209, 210 ], }, /* 223000.n.(l:w) */
  229: { go: true, src: [ 209, 211 ], }, /* 223000.s.(l:e) */

  230: { go: true, src: [ 209, 212 ], }, /* 223000.w.(l:s) */
  231: { go: true, src: [ 209, 213 ], }, /* 223000.e.(l:n) */

  /*     double parallel solid dividers, for traffic or cross walks */
  232: { go: true, src: [ 209, 214 ], }, /* 223000.v.(l:w-e) */
  233: { go: true, src: [ 209, 215 ], }, /* 223000.h.(l:n-s) */

  /* 2230: corners, solid: 2-side divider (parallel) + road */

  /*   22301: inside corner L */
  234: { go: true, src: [ 209, 216 ], }, /* 22301.1.e-s.(l:n-e) */
  235: { go: true, src: [ 209, 217 ], }, /* 22301.2.s-w.(l:s-e) */
  236: { go: true, src: [ 209, 218 ], }, /* 22301.3.w-n.(l:s-w) */
  237: { go: true, src: [ 209, 219 ], }, /* 22301.4.n-e.(l:n-w) */

  /*   22302: outside  corner ` */
  238: { go: true, src: [ 209, 220 ], }, /* 22302.1.n-w.(l:s-w) */
  239: { go: true, src: [ 209, 221 ], }, /* 22302.2.w-s.(l:s-e) */
  240: { go: true, src: [ 209, 222 ], }, /* 22302.3.s-e.(l:n-e) */
  241: { go: true, src: [ 209, 223 ], }, /* 22302.4.e-n.(l:n-w) */

  /*   22303: thick stop line for intersections */
  242: { go: true, src: [ 229, 224 ], }, /* 22303.s */
  243: { go: true, src: [ 231, 225 ], }, /* 22303.e */
  244: { go: true, src: [ 228, 226 ], }, /* 22303.n */
  245: { go: true, src: [ 230, 227 ], }, /* 22303.w */
};
